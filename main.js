/*jshint esversion: 6 */

function render(){
  let content = document.getElementById('content');
  // Header
  let header = document.createElement('h1');
  header.innerHTML = "Tabletop calculator";
  content.appendChild(header);

  /*---------------------APPEND-MENU------------------------*/
  let newUnit = document.createElement('div');
  newUnit.setAttribute("id", "newUnit");

  /*-----------------------VALUES------------------------*/
  let inputValues = document.createElement('div');
  inputValues.setAttribute("id", "inputValues");

  // Amount
  let amountDiv = document.createElement('div');
  amountDiv.className = "numberInput";
  let unitAmount = document.createElement('input');
  unitAmount.setAttribute("value", "0");
  unitAmount.setAttribute("id", "amount");
  unitAmount.setAttribute("type", "number");
  let unitAmountLabel = document.createElement('label');
  unitAmountLabel.setAttribute("for", "amount");
  unitAmountLabel.innerHTML = "Amount:";
  amountDiv.appendChild(unitAmountLabel);
  amountDiv.appendChild(unitAmount);
  // Points
  let pointsDiv = document.createElement('div');
  pointsDiv.className = "numberInput";
  let unitPoints = document.createElement('input');
  unitPoints.setAttribute("value", "0");
  unitPoints.setAttribute("id", "points");
  unitPoints.setAttribute("type", "number");
  let unitPointsLabel = document.createElement('label');
  unitPointsLabel.setAttribute("for", "points");
  unitPointsLabel.innerHTML = "Points:";
  pointsDiv.appendChild(unitPointsLabel);
  pointsDiv.appendChild(unitPoints);

  inputValues.appendChild(amountDiv);
  inputValues.appendChild(pointsDiv);
  /*-----------------------VALUES-END--------------------------*/

  /*-------------------------UNIT-DATA--------------------------*/
  let unitData = document.createElement('div');
  unitData.setAttribute("id", "unitData");

  let unitName = document.createElement('input');
  unitName.setAttribute("id", "unitName");
  unitName.setAttribute("placeholder", "Name:");

  let unitDescription = document.createElement('textarea');
  unitDescription.setAttribute("id", "unitDescription");
  unitDescription.setAttribute("placeholder", "Additional info:");

  unitData.appendChild(unitName);
  unitData.appendChild(unitDescription);
  /*------------------------UNIT-DATA- END-------------------*/


  /*-----------------------SUBMIT-DATA------------------------*/
  let submitData = document.createElement('div');
  submitData.setAttribute("id", "submitData");
  let submit = document.createElement('button');
  submit.addEventListener("click", addUnits);
  submit.innerHTML = "ADD";
  submitData.appendChild(submit);
  /*--------------------SUBMIT-DATA-END--------------------*/

  newUnit.appendChild(inputValues);
  newUnit.appendChild(unitData);
  newUnit.appendChild(submitData);
    /*-------------------APPEND-MENU-END------------------*/

    /*---------------------------UNITS-----------------------------*/
    let units = document.createElement('div');
    units.setAttribute("id", "units");
    let unitsHeader = document.createElement('h2');
    unitsHeader.innerHTML = "Units:";
    units.appendChild(unitsHeader);
    /*-------------------------UNITS-END------------------------*/

    /*---------------------------TOTAL-----------------------------*/
    let total = document.createElement('div');
    total.setAttribute("id", "total");

    let totalHeader = document.createElement('h2');
    totalHeader.innerHTML = "Total:";

    let totalAmount = document.createElement('p');
    totalAmount.setAttribute("id", "totalAmount");
    totalAmount.innerHTML = "0pts";

    total.appendChild(totalHeader);
    total.appendChild(totalAmount);

    /*-------------------------TOTAL-END------------------------*/

  content.appendChild(newUnit);
  content.appendChild(units);
  content.appendChild(total);

}

function addUnits(){
  let getName = document.getElementById('unitName').value;
  let getDescription = document.getElementById('unitDescription').value;
  let getAmount = document.getElementById('amount').value;
  let getPoints = document.getElementById('points').value;

  // CURRENT TOTAL POINTS
  let currentTotal = document.getElementById('totalAmount');
  currentTotal.innerHTML = currentTotal.innerHTML.replace("pts", "");
  currentTotal.innerHTML =  Number(currentTotal.innerHTML) + Number(getAmount) * Number(getPoints) + "pts";

  // Wrapper for whole unit
  let setUnit = document.createElement('div');
  setUnit.className = 'listUnit';

  // Wrapper for text values
  let setValues = document.createElement('div');
  setValues.className = "unitValues";

  // AMOUNT
  let setUnitAmount = document.createElement('p');
  setUnitAmount.innerHTML = getAmount;
  // NAME
  let setUnitName = document.createElement('p');
  setUnitName.innerHTML = getName;
  // DESCRIPTION
  let setUnitDescription = document.createElement('p');
  setUnitDescription.innerHTML = getDescription;
  // UNIT COST FOR UI
  let setUnitPoints = document.createElement('p');
  // UNIT COST FOR CALCULATIONS
  let getUnitPoints = getPoints;
  setUnitPoints.innerHTML = getPoints + "pts";

  setValues.appendChild(setUnitAmount);
  setValues.appendChild(setUnitName);
  setValues.appendChild(setUnitDescription);
  setValues.appendChild(setUnitPoints);


  // CONTROLS
  let numberControls = document.createElement('div');
  numberControls.className = "controls";
  // CONTROLS - MINUS
  let minusAmount = document.createElement('button');
  minusAmount.addEventListener("click", reduceAmount);
  minusAmount.innerHTML = "-";
  // CONTROLS - PLUS
  let plusAmount = document.createElement('button');
  plusAmount.addEventListener("click", increaseAmount);
  plusAmount.innerHTML = "+";

  numberControls.appendChild(minusAmount);
  numberControls.appendChild(plusAmount);

  let remove =document.createElement('button');
  remove.addEventListener("click", removeThis);
  remove.innerHTML = "X";
  remove.className = "removeButton";

  setUnit.appendChild(setValues);
  setUnit.appendChild(numberControls);
  setUnit.appendChild(remove);

  document.getElementById('units').appendChild(setUnit);

  // Unit removal function

  function removeThis(){
    let thisAmount = this.parentNode.firstChild.firstChild.innerHTML;
    let thisPoints = this.parentNode.firstChild.lastChild.innerHTML;
    thisPoints = thisPoints.replace("pts", "");
    let thisTotal = Number(thisAmount) * Number(thisPoints);
    this.parentNode.parentNode.nextSibling.lastChild.innerHTML = this.parentNode.parentNode.nextSibling.lastChild.innerHTML.replace("pts", "");
    this.parentNode.parentNode.nextSibling.lastChild.innerHTML = Number(this.parentNode.parentNode.nextSibling.lastChild.innerHTML) - Number(thisTotal) + "pts";
    let deleteThis = this.parentNode;
    this.parentNode.parentNode.removeChild(deleteThis);
  }

  // Unit control + / - control button functions

  function reduceAmount(){
    if (this.parentNode.parentNode.firstChild.firstChild.innerHTML > 0) {
      let currentAmount = Number(this.parentNode.parentNode.firstChild.firstChild.innerHTML);
      currentAmount--;
      this.parentNode.parentNode.firstChild.firstChild.innerHTML = currentAmount;
      console.log(getUnitPoints);
      let newTotal = Number(currentTotal.innerHTML.replace("pts","")) - Number(getUnitPoints);
      currentTotal.innerHTML = newTotal + "pts";
    }
  }

  function increaseAmount(){
    let currentAmount = Number(this.parentNode.parentNode.firstChild.firstChild.innerHTML);
    currentAmount++;
    this.parentNode.parentNode.firstChild.firstChild.innerHTML = currentAmount;
    console.log(getUnitPoints);
    let newTotal = Number(currentTotal.innerHTML.replace("pts","")) + Number(getUnitPoints);
    currentTotal.innerHTML = newTotal + "pts";
  }

}


window.onload = render();
